#!/bin/bash
#
# cern-import-certs-java - tool to import CERN certificates in system wide java certificate stores for different java versions
#
# 08.11.2013 jaroslaw.polok@cern.ch - v. 0.1
# 21.11.2013 jaroslaw.polok@cern.ch - v. 0.2
# 13.12.2013 jaroslaw.polok@cern.ch - v. 0.3
# 25.03.2014 jaroslaw.polok@cern.ch - v. 0.4
# 16.05.2018 jaroslaw.polok@cern.ch - v. 0.5 
#
# CERN certs to import
#

CERTSHA256FPRINTS=(
"87:C8:E4:9F:21:A8:53:91:0C:D4:BE:FD:7A:E9:6C:92:06:AD:0B:16:7E:84:B4:05:8B:E0:07:52:29:F8:69:A0"
"18:7F:4F:7B:13:15:EB:D2:B7:25:47:C1:AC:CF:2E:C4:07:7D:51:C1:4A:0C:E5:74:90:4A:9F:DA:EA:8A:32:0C"
"59:9C:13:71:4B:D4:91:5E:BA:8A:C7:E7:88:DD:50:9D:B9:EA:15:BB:D6:AE:DF:23:0D:17:35:A3:07:26:2C:AD"
"99:50:AD:A2:95:6F:13:D3:3E:F6:94:45:E6:3D:1A:E0:37:F8:07:51:9E:B9:08:0D:2D:89:B1:62:E0:CA:02:7F"
"58:0E:70:B4:AF:BE:DF:7F:7D:31:08:AE:B3:F4:29:E2:A6:8A:94:88:92:58:29:AA:7C:0C:D5:17:2C:50:0B:E3"
)

CERTSHA1FPRINTS=(
"DA:D8:7F:63:95:90:A1:E4:D4:1D:B9:48:3D:F4:C3:5C:FC:6B:BF:A3"
"57:6E:90:75:DD:66:92:8B:F2:5B:D9:52:D6:37:EA:AD:80:84:CC:9F"
"63:C2:81:78:6D:94:14:D7:51:56:33:89:21:64:F1:85:A6:B0:D1:E0"
"1D:04:A6:38:0C:DE:0B:FD:2F:59:E3:05:2C:F7:9E:07:CA:1C:16:B2"
"69:93:8A:AB:92:79:A2:03:3E:71:B3:2B:29:FC:65:06:69:C3:4E:D3"
)

CERTMD5FPRINTS=(
"A2:CE:DC:7C:F5:60:D7:2C:5E:A5:29:74:9D:52:E9:49"
"E6:8A:DC:81:39:BA:52:33:8C:EF:A3:13:12:AE:CB:67"
"4E:BB:91:96:B6:95:0A:66:DC:FC:05:52:BA:F2:1D:35"
"7D:97:5F:E0:F2:4B:95:9A:C5:E4:E3:2D:C1:EA:9E:99"
"F5:89:52:02:31:C7:B8:63:F6:21:80:4D:92:4F:17:05"
)

CERTPEMFILES=(
"/etc/pki/tls/certs/CERN_Root_CA.pem"
"/etc/pki/tls/certs/CERN_Root_Certification_Authority_2.pem"
"/etc/pki/tls/certs/CERN_Grid_Certification_Authority.pem"
"/etc/pki/tls/certs/CERN_Certification_Authority.pem"
"/etc/pki/tls/certs/CERN_Certification_Authority(1).pem"
)

CERTALIASES=(
"cernrootca"
"cernrootcertificationauthority2"
"cerngridcertificationauthority"
"cerncertificationauthority"
"cerncertificationauthority_1_"
)


SEARCHPATH="/usr/lib/jvm"

[ ! -z "$1" ] && SEARCHPATH=$1

#
# We need openjdk 1.7.0/1.8.0 for system cert store, on SLC6, no system-wide java cert store on 5.
#
SYSKEYTOOL=`/usr/bin/find /usr/lib/jvm/java-1.{7,8}.0-openjdk* -name keytool 2>/dev/null | /usr/bin/head -1` 
[ -z $SYSKEYTOOL ] && SYSKEYTOOL="(none)"  
SYSCERTSTORE="/etc/pki/java/cacerts"
SYSKEYSTOREOPTS="-importcert -noprompt"
SYSKEYSTOREPASS="changeit"

if [ -x $SYSKEYTOOL -a -r $SYSCERTSTORE -a -z "$1" ]; then

 for (( I=0; I<${#CERTSHA1FPRINTS[@]}; I++)) do

  RES=`echo $SYSKEYSTOREPASS | $SYSKEYTOOL -list -keystore $SYSCERTSTORE 2>&1 | /bin/grep -Pc "${CERTSHA256FPRINTS[$I]}|${CERTSHA1FPRINTS[$I]}|${CERTMD5FPRINTS[$I]}"`

  if [ $RES -eq 0 -a -r ${CERTPEMFILES[$I]} ]; then

   echo "Adding ${CERTPEMFILES[$I]} to $CERTSTORE"
   echo $SYSKEYSTOREPASS | $SYSKEYTOOL $SYSKEYSTOREOPTS -keystore $SYSCERTSTORE -file ${CERTPEMFILES[$I]} -alias ${CERTALIASES[$I]}

  fi

 done

fi


#
# java-1.5.0-gcj keytool seems broken: does not allow to import non-interactively ...
# well .. nobody uses this in 2013 ... right ? ...
#
for CERTSTORE in `/usr/bin/find $SEARCHPATH -name cacerts -type f 2>/dev/null | /bin/grep -v java-1.5.0-gcj`; do

   KEYTOOL=${CERTSTORE//lib\/security\/cacerts/bin\/keytool}
   [ -z $KEYTOOL ] && KEYTOOL="(none)" 

      if [ -x $KEYTOOL ]; then

         for (( I=0; I<${#CERTSHA1FPRINTS[@]}; I++)) do

            case $KEYTOOL in

               *java-1.5.0-gcj*)
                  KEYSTOREPASS=""	
                  KEYSTOREOPTS="-import -noprompt"
                  ;;

               *)
                  KEYSTOREPASS="changeit"
                  KEYSTOREOPTS="-importcert -noprompt"
                  ;;

            esac

            RES=`echo $KEYSTOREPASS | $KEYTOOL -list -keystore $CERTSTORE  2>&1 | /bin/grep -Pc "${CERTSHA256FPRINTS[$I]}|${CERTSHA1FPRINTS[$I]}|${CERTMD5FPRINTS[$I]}"`

            if [ $RES -eq 0 -a -r ${CERTPEMFILES[$I]} ]; then

	       echo "Adding ${CERTPEMFILES[$I]} to $CERTSTORE"	
               echo $KEYSTOREPASS | $KEYTOOL $KEYSTOREOPTS -keystore $CERTSTORE -file ${CERTPEMFILES[$I]} -alias ${CERTALIASES[$I]}

            fi

         done 
      fi
done

exit 0
