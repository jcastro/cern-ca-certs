#!/bin/bash

GRACEDAYS=40

ret=0
cd src
for cert in *.crt; do
  enddate=`openssl x509 -in $cert -inform DER -enddate -noout | sed -e 's#notAfter=##'`

  certdate=`date -d "${enddate}" '+%s'`
  today=`date '+%s'`
  diff="$((${certdate}-${today}))"

  echo -n "${cert}: "
  if [[ "${diff}" -lt "$((${GRACEDAYS}*24*3600))" ]]; then
    ret=$(($ret+1))
    if [[ "${diff}" -lt "0" ]]; then
      echo -e "\e[1m\e[31mcertificate expired on ${enddate}!\e[0m"
    else
      echo -e "\e[1m\e[31mcertificate will expire in $((${diff}/3600/24)) days, on ${enddate}.\e[0m"
    fi
  else
    echo "certificate expires on ${enddate}."
  fi
done

exit $ret
